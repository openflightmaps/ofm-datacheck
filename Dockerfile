FROM debian
RUN apt-get update && apt-get install -y libxml2-utils ca-certificates
COPY ofmxcheck /
ENTRYPOINT ["/ofmxcheck"]
