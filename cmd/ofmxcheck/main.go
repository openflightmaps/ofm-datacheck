package main

import (
	"bufio"
	"bytes"
	"context"
	"encoding/csv"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"regexp"
	"sort"
	"strings"
	"sync"
	"time"

	"cloud.google.com/go/storage"
	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"github.com/joho/godotenv"
	"github.com/jwkohnen/airac"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/common/expfmt"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/openflightmaps/discovery/pkg/discovery"
	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx"
	ofmx_types "gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
	"golang.org/x/net/html/charset"
	"gopkg.in/go-playground/validator.v9"
	en_translations "gopkg.in/go-playground/validator.v9/translations/en"
)

/*
	TODO: REFACTOR, this is ugly spaghetti code
*/

func AddToFmap(fmap ofmx_types.FeatureMap, features []ofmx_types.Feature) {
	for _, f := range features {
		if f.Uid().OriginalMid() != "" && f.Uid().OriginalMid() != f.Uid().Hash() {
			log.Printf("WARNING: mid mismatch: %s %s/%s\n", f.Uid().String(), f.Uid().OriginalMid(), f.Uid().Hash())
		}
		_, ok := fmap[f.Uid().Hash()]
		if ok {
			log.Printf("WARNING: duplicate feature: %s\n", f.Uid().String())
			continue
		}
		//log.Printf("Loaded: %s\n", f.Uid().String())
		fmap[f.Uid().Hash()] = f
	}
}

var matches map[string]string

func init() {
	matches = make(map[string]string, 0)
}

func parseLine(line string) {
	re := regexp.MustCompile("^(.*): element (.*): Schemas validity error : Element '(.*)': (.*)")
	match := re.FindStringSubmatch(line)
	if len(match) > 0 {
		//fmt.Println(match[4])
		//matches[match[4]] = match[1]
	} else {
		fmt.Println("NO MATCH")
		fmt.Println(line)
		fmt.Println()
	}
}

// dropCR drops a terminal \r from the data.
func dropCR(data []byte) []byte {
	if len(data) > 0 && data[len(data)-1] == '\r' {
		return data[0 : len(data)-1]
	}
	return data
}

func GetSplitFunc(fname string) bufio.SplitFunc {
	fname = "\n" + fname + ":"

	sf := func(data []byte, atEOF bool) (advance int, token []byte, err error) {
		if atEOF && len(data) == 0 {
			return 0, nil, nil
		}
		if i := bytes.Index(data, []byte(fname)); i >= 0 {
			// We have a full newline-terminated line.
			return i + 1, dropCR(data[0:i]), nil
		}
		// If we're at EOF, we have a final, non-terminated line. Return it.
		if atEOF {
			return len(data), dropCR(data), nil
		}
		// Request more data.
		return 0, nil, nil
	}
	return sf
}

// DownloadFile will download a url to a local file. It's efficient because it will
// write as it downloads and not load the whole file into memory.
func downloadFile(filepath string, url string) error {

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}

func downloadOfmx(dev bool, region, filename string) error {
	var env string
	if dev {
		env = "-dev"
	}

	// TODO: this should be reverted to 7 days ahead
	airac := airac.FromDate(time.Now().AddDate(0, 0, 14)).String()

	regionShort := region[0:2]

	bucket := fmt.Sprintf("snapshots%s.openflightmaps.org", env)
	path := fmt.Sprintf("live/%s/ofmx/%s/latest/isolated", airac, region)

	if err := downloadFromGs(bucket, fmt.Sprintf("%s/ofmx_%s.ofmx", path, regionShort), filename); err != nil {
		return err
	}
	if err := downloadFromGs(bucket, fmt.Sprintf("%s/ofmx_%s_ofmShapeExtension.xml", path, regionShort), filename+".shape"); err != nil {
		return err
	}
	return nil
}

func uploadValidationArtifact(dev bool, region, artifactName string) (io.WriteCloser, error) {
	var env string
	if dev {
		env = "-dev"
	}

	bucket := fmt.Sprintf("snapshots%s.openflightmaps.org", env)
	path := fmt.Sprintf("validation/ofmx/%s", region)

	return streamToGs(bucket, fmt.Sprintf("%s/%s", path, artifactName))
}

func downloadFromGs(bucketName, objectName, destination string) error {
	fmt.Println(objectName)
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		return err
	}

	bkt := client.Bucket(bucketName)

	obj := bkt.Object(objectName)
	r, err := obj.NewReader(ctx)
	if err != nil {
		return err
	}
	defer r.Close()

	w, err := os.Create(destination)
	if err != nil {
		return err
	}
	defer w.Close()

	if _, err := io.Copy(w, r); err != nil {
		return err
	}

	return nil
}

func streamToGs(bucketName, objectName string) (io.WriteCloser, error) {
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		return nil, err
	}

	bkt := client.Bucket(bucketName)
	// Next check if the bucket exists
	if _, err = bkt.Attrs(ctx); err != nil {
		return nil, err
	}

	obj := bkt.Object(objectName)
	w := obj.NewWriter(ctx)

	return w, nil
}

func processXmlLint(w io.WriteCloser, xmlName string) error {
	cmd := exec.Command("xmllint", "--noout", "--schema", "OFMX-Snapshot.xsd", xmlName)
	errors := 0

	stderrIn, _ := cmd.StderrPipe()
	//pr, pw := io.Pipe()
	cmd.Stdout = os.Stdout
	//cmd.Stderr = pw

	var wg sync.WaitGroup
	wg.Add(1)

	go func() {
		defer w.Close()
		//defer pr.Close()
		scanner := bufio.NewScanner(stderrIn)
		scanner.Split(GetSplitFunc(xmlName))
		for scanner.Scan() {
			parseLine(scanner.Text())
			if _, e := w.Write(scanner.Bytes()); e != nil {
				fmt.Println(e)
			}

			if _, e := w.Write([]byte("\n")); e != nil {
				fmt.Println(e)
			}
			errors++
		}
		wg.Done()
	}()

	if err := cmd.Start(); err != nil {
		return err
	}

	wg.Wait()

	if err := cmd.Wait(); err != nil {
		return err
	}
	return nil
}

type validationError struct {
	Id    string
	Field string
	Error string
	Value interface{}
}

func loadOfmx(fName string) (ofmx_types.FeatureMap, error) {
	var snap ofmx.OfmxSnapshot
	var errorList []validationError
	validate := validator.New()
	en := en.New()
	uni := ut.New(en, en)
	trans, _ := uni.GetTranslator("en")
	en_translations.RegisterDefaultTranslations(validate, trans)
	ofmx_types.RegisterValidators(validate, trans)

	fmap := make(ofmx_types.FeatureMap, 0)

	fOfmx, err := os.Open(fName)
	if err != nil {
		return nil, err
	}
	defer fOfmx.Close()

	ofmxDecoder := xml.NewDecoder(fOfmx)
	ofmxDecoder.Strict = true
	ofmxDecoder.CharsetReader = charset.NewReaderLabel

	if err = ofmxDecoder.Decode(&snap); err != nil {
		return nil, err
	}
	AddToFmap(fmap, snap.Features)
	ofmx.UpdateReferences(fmap)

	for _, f := range snap.Features {

		uid := f.Uid()

		mid := f.Uid().OriginalMid()
		if uid.Hash() != mid {
			fmt.Printf("%s: %s/%s\n", uid.String(), uid.Hash(), mid)
		}

		err := validate.Struct(f)
		if err != nil {
			// this check is only needed when your code could produce
			// an invalid value for validation such as interface with nil
			// value most including myself do not usually have code like this.
			if _, ok := err.(*validator.InvalidValidationError); ok {
				fmt.Println(err)
				continue
			}

			//errs := err.(validator.ValidationErrors)
			//fmt.Println(errs)

			for _, err := range err.(validator.ValidationErrors) {
				errorList = append(errorList, validationError{Id: uid.String(), Field: err.StructNamespace(), Value: err.Value(), Error: err.Translate(trans)})

			}

			//log.Printf("validation failed: %v", err)
		}
	}

	for _, e := range errorList {
		log.Printf("%s: %s '%v'\n", e.Id, e.Error, e.Value)
	}
	return fmap, nil
}

func formatLimit(code, uom, val string) string {
	p := ""
	switch code {
	case "ALT":
		if uom != "FT" {
			p = "INVALID:"
		}
		return fmt.Sprintf("%s%s %s MSL", p, val, uom)
	case "STD":
		if uom != "FL" {
			p = "INVALID:"
		}
		return fmt.Sprintf("%s%s%s", p, uom, val)
	case "HEI":
		if uom != "FT" {
			p = "INVALID:"
		}
		if val == "0" {
			return fmt.Sprintf("%sGND", p)
		}
		return fmt.Sprintf("%s%s %s GND", p, val, uom)
	default:
		if code == "" && uom == "" && val == "" {
			return ""
		}
		return fmt.Sprintf("INVALID: %s%s (%s)", val, uom, code)
	}
}

func convertToAirspacesCsv(w io.Writer, fmap ofmx_types.FeatureMap) error {

	wNavCsv := csv.NewWriter(w)
	header := []string{"codeId", "txtName", "codeType", "txtLocalType", "codeClass", "min", "lower", "max", "upper", "codeSelAvbl", "txtRmk"}
	wNavCsv.Write(header)

	lines := [][]string{}
	for _, f := range fmap {
		switch v := f.(type) {
		case *ofmx_types.Ase:
			lines = append(lines, []string{v.AseUid.CodeId, v.TxtName, v.AseUid.CodeType, v.TxtLocalType, v.CodeClass,
				formatLimit(v.CodeDistVerLower, v.UomDistVerLower, v.ValDistVerLower),
				formatLimit(v.CodeDistVerMnm, v.UomDistVerMnm, v.ValDistVerMnm),
				formatLimit(v.CodeDistVerUpper, v.UomDistVerUpper, v.ValDistVerUpper),
				formatLimit(v.CodeDistVerMax, v.UomDistVerMax, v.ValDistVerMax),
				v.CodeSelAvbl,
				v.TxtRmk})
		}
	}
	sort.Slice(lines, func(i, j int) bool {
		return lines[i][0] < lines[j][0]
	})
	wNavCsv.WriteAll(lines)
	wNavCsv.Flush()
	return nil
}

func convertToNavigationCsv(w io.Writer, fmap ofmx_types.FeatureMap) error {

	wNavCsv := csv.NewWriter(w)
	header := []string{"name", "id", "frequency", "hours", "lat", "long", "elevation", "remarks"}
	wNavCsv.Write(header)

	lines := [][]string{}
	for _, f := range fmap {
		switch v := f.(type) {
		case *ofmx_types.Vor:
			lines = append(lines, []string{v.TxtName, v.VorUid.CodeId, v.ValFreq + " " + v.UomFreq, "unknown", v.VorUid.GeoLat, v.VorUid.GeoLong, v.ValElev, v.TxtRmk})
		case *ofmx_types.Dme:
			lines = append(lines, []string{v.TxtName, v.DmeUid.CodeId, v.CodeChannel, "unknown", v.DmeUid.GeoLat, v.DmeUid.GeoLong, v.ValElev, v.TxtRmk})
		case *ofmx_types.Ndb:
			lines = append(lines, []string{v.TxtName, v.NdbUid.CodeId, v.ValFreq + " " + v.UomFreq, "unknown", v.NdbUid.GeoLat, v.NdbUid.GeoLong, v.ValElev, v.TxtRmk})

		}
	}
	sort.Slice(lines, func(i, j int) bool {
		return lines[i][0] < lines[j][0]
	})
	wNavCsv.WriteAll(lines)
	wNavCsv.Flush()
	return nil
}

func convertToDpnCsv(w io.Writer, fmap ofmx_types.FeatureMap) error {

	wNavCsv := csv.NewWriter(w)
	header := []string{"name", "id", "type", "lat", "long", "airport_association", "remarks"}
	wNavCsv.Write(header)

	lines := [][]string{}
	for _, f := range fmap {
		switch v := f.(type) {
		case *ofmx_types.Dpn:
			ahp := ""
			if v.AhpUidAssoc != nil {
				ahp = v.AhpUidAssoc.CodeId
			}
			lines = append(lines, []string{v.TxtName, v.DpnUid.CodeId, v.CodeType, v.DpnUid.GeoLat, v.DpnUid.GeoLong, ahp, v.TxtRmk})

		}
	}
	sort.Slice(lines, func(i, j int) bool {
		return lines[i][0] < lines[j][0]
	})
	wNavCsv.WriteAll(lines)
	wNavCsv.Flush()
	return nil
}

func convertFreqToCsv(w io.Writer, fmap ofmx_types.FeatureMap) error {

	wFqyCsv := csv.NewWriter(w)
	header := []string{"callsign", "lang", "freq", "type", "remarks", "servicename", "unittype", "servicetype", "ad"}
	wFqyCsv.Write(header)

	lines := [][]string{}
	for _, f := range fmap {
		switch v := f.(type) {
		case *ofmx_types.Fqy:
			var ad string
			uni := v.FqyUid.SerUid.UniUid.Ref()
			if uni != nil && uni.AhpUid != nil {
				ad = uni.AhpUid.CodeId
			}
			lines = append(lines, []string{v.Cdl[0].TxtCallSign, v.Cdl[0].CodeLang, v.FqyUid.ValFreqTrans, v.CodeType, v.TxtRmk, v.FqyUid.SerUid.UniUid.TxtName, v.FqyUid.SerUid.UniUid.CodeType, v.FqyUid.SerUid.CodeType, ad})

		}
	}
	sort.Slice(lines, func(i, j int) bool {
		return lines[i][0] < lines[j][0]
	})
	wFqyCsv.WriteAll(lines)
	wFqyCsv.Flush()
	return nil
}

func createStats(region string, fmap ofmx_types.FeatureMap) error {
	validate := validator.New()
	en := en.New()
	uni := ut.New(en, en)
	trans, _ := uni.GetTranslator("en")
	en_translations.RegisterDefaultTranslations(validate, trans)
	ofmx_types.RegisterValidators(validate, trans)

	for _, f := range fmap {
		s := f.Uid().String()
		t := strings.Split(s, "|")[0][0:3]

		uid := f.Uid()

		mid := f.Uid().OriginalMid()
		if uid.Hash() != mid {
			fmt.Printf("%s: %s/%s\n", uid.String(), uid.Hash(), mid)
		}

		err := validate.Struct(f)
		e := "false"
		if err != nil {
			e = "true"
		}
		var subtype string

		switch t := f.(type) {
		case *ofmx_types.Ase:
			subtype = t.AseUid.CodeType
		case *ofmx_types.Ahp:
			subtype = t.CodeType
		case *ofmx_types.Dpn:
			subtype = t.CodeType
		}
		labels := prometheus.Labels{"region": region, "type": t, "subtype": subtype, "error": e}
		pCount.With(labels).Inc()
	}
	return nil
}

func processRegion(dev bool, region string) error {
	xmlName := region + "_ofmx.xml"

	wVal, err := uploadValidationArtifact(dev, region, "validation.log")
	if err != nil {
		return err
	}
	defer wVal.Close()

	// log to validation.log and stderr
	logw := io.MultiWriter(os.Stderr, wVal)
	log.SetOutput(logw)

	if err := downloadOfmx(dev, region, xmlName); err != nil {
		return err
	}

	log.Println("Starting XMLLINT")
	wXml, err := uploadValidationArtifact(dev, region, "xmllint.log")
	if err != nil {
		return err
	}
	defer wXml.Close()

	if err := processXmlLint(wXml, xmlName); err != nil {
		log.Println("xmllint failed")
		log.Println(err)
	}

	fmap, err := loadOfmx(xmlName)
	if err != nil {
		return err
	}

	wAse, err := uploadValidationArtifact(dev, region, "airspaces.csv")
	if err != nil {
		panic(err)
	}
	defer wAse.Close()
	convertToAirspacesCsv(wAse, fmap)

	wNav, err := uploadValidationArtifact(dev, region, "navaids.csv")
	if err != nil {
		panic(err)
	}
	defer wNav.Close()
	convertToNavigationCsv(wNav, fmap)

	wDp, err := uploadValidationArtifact(dev, region, "waypoints.csv")
	if err != nil {
		panic(err)
	}
	defer wDp.Close()

	convertToDpnCsv(wDp, fmap)

	wFq, err := uploadValidationArtifact(dev, region, "frequencies.csv")
	if err != nil {
		panic(err)
	}
	defer wFq.Close()

	convertFreqToCsv(wFq, fmap)

	createStats(region, fmap)

	// try some logging
	err = wXml.Close()
	if err != nil {
		log.Println(err)
	}
	err = wNav.Close()
	if err != nil {
		log.Println(err)
	}
	err = wDp.Close()
	if err != nil {
		log.Println(err)
	}
	err = wFq.Close()
	if err != nil {
		log.Println(err)
	}

	log.SetOutput(os.Stderr)
	err = wVal.Close()
	if err != nil {
		log.Println(err)
	}
	return nil
}

func generateWikiPage(dev bool, regions []string, wikiFile string) error {
	var env string
	wikiPage := "results"
	if dev {
		env = "-dev"
		wikiPage = "results-dev"
	}

	dls := [][]string{
		{"validation log", "validation.log"},
		{"xmllint", "xmllint.log"},
		{"airspaces.csv", "airspaces.csv"},
		{"navaids.csv", "navaids.csv"},
		{"waypoints.csv", "waypoints.csv"},
		{"frequencies.csv", "frequencies.csv"},
	}
	md := "These are the current results:\n\n"
	for _, r := range regions {
		md += fmt.Sprintf("# %s\n\n", r)
		for _, f := range dls {
			md += fmt.Sprintf("<code>[%s](https://storage.googleapis.com/snapshots%s.openflightmaps.org/validation/ofmx/%s/%s)</code>\n&hairsp;\n", f[0], env, r, f[1])
		}
	}
	git, err := gitlab.NewClient(os.Getenv("GITLAB_TOKEN"))
	if err != nil {
		fmt.Printf("Failed to create client: %v;n", err)
	}
	_, _, err = git.Wikis.EditWikiPage("openflightmaps/ofm-datacheck", wikiPage, &gitlab.EditWikiPageOptions{Content: &md, Title: &wikiPage})
	if err != nil {
		fmt.Printf("Could not edit wiki page: %v;n", err)
	}
	return ioutil.WriteFile(wikiFile, []byte(md), os.ModePerm)
}

var pCount *prometheus.GaugeVec

func _map(vs []string, f func(string) string) []string {
	vsm := make([]string, len(vs))
	for i, v := range vs {
		vsm[i] = f(v)
	}
	return vsm
}

func processRegions(dev bool) {

	var disco *discovery.Discovery
	var err error
	if dev {
		disco, err = discovery.NewDiscovery(discovery.Dev(), discovery.Unreleased())
	} else {
		disco, err = discovery.NewDiscovery(discovery.Unreleased())
	}

	if err != nil {
		panic(err)
	}
	regions := _map(disco.Regions(), func(s string) string {
		return strings.ToLower(s)
	})

	// download Schema
	if err := downloadFile("OFMX-Snapshot.xsd", "https://schema.openflightmaps.org/0.1/OFMX-Snapshot.xsd"); err != nil {
		panic(err)
	}
	if err := downloadFile("OFMX-Features.xsd", "https://schema.openflightmaps.org/0.1/OFMX-Features.xsd"); err != nil {
		panic(err)
	}
	if err := downloadFile("OFMX-DataTypes.xsd", "https://schema.openflightmaps.org/0.1/OFMX-DataTypes.xsd"); err != nil {
		panic(err)
	}

	generateWikiPage(dev, regions, "wiki.md")

	// init prometheus
	labels := []string{"region", "type", "subtype", "error"}
	pCount = prometheus.NewGaugeVec(prometheus.GaugeOpts{Name: "ofmx_features_count"}, labels)

	for _, region := range regions {
		fmt.Printf("Processing %s\n", region)
		if err := processRegion(dev, region); err != nil {
			fmt.Printf("Validation failed for %s\n", region)
			fmt.Println(err)
		}
	}

	/*
		Prometheus Stats: ugly hack
	*/

	wStats, err := uploadValidationArtifact(dev, "all", "stats.txt")
	if err != nil {
		panic(err)
	}
	defer wStats.Close()

	r := prometheus.NewRegistry()
	prometheus.DefaultRegisterer = r
	prometheus.DefaultGatherer = r
	prometheus.MustRegister(pCount)
	mfs, err := prometheus.DefaultGatherer.Gather()
	if err != nil {
		panic(err)
	}
	for _, mf := range mfs {
		if _, err := expfmt.MetricFamilyToText(wStats, mf); err != nil {
			panic(err)
		}
	}
}

func main() {
	godotenv.Load()

	os.Mkdir("temp", os.ModeDir)
	os.Chdir("temp")
	processRegions(false)
}
