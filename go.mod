module gitlab.com/openflightmaps/ofm-datacheck

go 1.13

require (
	cloud.google.com/go v0.61.0 // indirect
	cloud.google.com/go/storage v1.10.0
	github.com/go-playground/locales v0.13.0
	github.com/go-playground/universal-translator v0.17.0
	github.com/google/go-cmp v0.5.1 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/jwkohnen/airac v1.0.5
	github.com/prometheus/client_golang v1.6.0
	github.com/prometheus/common v0.9.1
	github.com/xanzy/go-gitlab v0.38.1
	gitlab.com/openflightmaps/discovery v0.0.2
	gitlab.com/openflightmaps/go-ofmx v0.0.11
	golang.org/x/net v0.0.0-20200707034311-ab3426394381
	golang.org/x/tools v0.0.0-20200727233628-55644ead90ce // indirect
	google.golang.org/genproto v0.0.0-20200728010541-3dc8dca74b7b // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0
)

//replace gitlab.com/openflightmaps/go-ofmx => ../go-ofmx
